# Tabbar页面

注: 微信上默认使用[skyline](https://developers.weixin.qq.com/miniprogram/dev/framework/runtime/skyline/migration/)渲染页面

- Skyline 使用注意事项

```
1. 此代码依赖基础库依赖 3.0.2 对应微信版本 8.0.40 (2023-07-20) 90%+以上设备。否则打开小程序微信默认展示页面 “升级微信”, 另：基础库版本 >= 2.29.2。

2. 设置

manifest.json > "lazyCodeLoading": "requiredComponents"
manifest.json > "rendererOptions": 
{
  "skyline": {
    "defaultDisplayBlock": true,
    // 可不设置, 微信默认投放小程序是使用AB实验, 默认部分用户为webviwe模式
    "disableABTest": true
  }
}

manifest.json 或 pages.json > "renderer": "skyline"
manifest.json 或 pages.json > "componentFramework": "glass-easel"
page.json > "navigationStyle": "custom"
// Skyline 不支持页面全局滚动, 在需要滚动的区域使用 scroll-view 与 WebView 保持兼容
pages.json > "disableScroll": true
pages.json > "navigationStyle": "custom"

3. 开发者工具使用Nightly版本, 开启ES6 -> ES5, Skyline渲染
```

- [Skyline 基础组件支持与差异](https://developers.weixin.qq.com/miniprogram/dev/framework/runtime/skyline/component.html)

- [Skyline WXSS 样式支持与差异](https://developers.weixin.qq.com/miniprogram/dev/framework/runtime/skyline/wxss.html)

- [Skyline 最佳实践](https://developers.weixin.qq.com/miniprogram/dev/framework/runtime/skyline/migration/best-practice.html)
