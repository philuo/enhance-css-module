# 增强css-module体验

本示例为微信小程序skyline版本, 无需打开小程序开发者工具。

执行下面的脚本后，看dist目录下pages中的wxml & wxss & js的类名是否符合阁下的预期

```bash
npm run dev:mp-weixin
# 或者下面这个👇
npm run build:mp-weixin
```

```
<style lang="scss" module> 无需书写$style 和 :class 开箱即用。
自动补全当前.vue文件静态class属性hash后缀，切无需担心全局类名被替换。

注意：
1. 空类：只有类名，没有描述例如 .test {}， 这种情况保持 class="test"
<template>
    <view class="test"></view>
</template>

2. <script> import 类模块，必须使用动态class了。建议是在<style>中通过 @import "xxx.scss"; 引入
```

